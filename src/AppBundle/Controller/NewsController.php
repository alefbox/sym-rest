<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends Controller
{
    /**
     * @param Request $request the request object
     * @return News[]
     */
    public function listAction(Request $request, $page)
    {
        $em = $this->get('doctrine')->getManager();
        
        $pager = array(
            'all' => 0,
            'page' => $page,
            'max_result' => 3,
            'count' => 0
        );
        
        // Получаем количество записей
        $pager['all'] = $em->createQuery("SELECT count(n.id) FROM AppBundle:News n")->getResult(\Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);
        
        // Получаем количество страниц
        $pager['count'] = ceil($pager['all']/$pager['max_result']);
        
        $DQL = $em->createQuery("SELECT n FROM AppBundle:News n");
        $DQL->setMaxResults($pager['max_result']);
        $DQL->setFirstResult(($pager['page']-1)*$pager['max_result']);

        $News = $DQL->getResult();

        $jsonContent = $this->get('my_serializer')->serialize(['news' => $News, 'pager' => $pager], 'json');
        
        $response = new Response($jsonContent);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
    
    /**
     * @param Request $request the request object
     * @param int $id
     * @return News[]
     */
    public function itemAction(Request $request, $id)
    {
        $News = $this->getDoctrine()->getRepository('AppBundle:News')->find($id);  

        $jsonContent = $this->get('my_serializer')->serialize($News, 'json');
        
        $response = new Response($jsonContent);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
}
