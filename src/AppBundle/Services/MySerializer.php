<?php
/**
 * MySerializer
 * 
 * 
 * @author Alef <alefbox@gmail.com>
 * @version 1.0
 */

namespace AppBundle\Services;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class MySerializer
{	    		
    /**
     * @param $items
     * @return []
     */	
    public function serialize($items, $format='json')
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($items, $format);
        
        return $jsonContent;
    }
}
